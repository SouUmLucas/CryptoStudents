﻿using System.Windows;
using static System.Windows.Visibility;

namespace CryptoStudent
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        Crypto Authenticate;
        DBConnect Connetion;
        Authentication auth;

        public MainWindow()
        {
            InitializeComponent();

            Authenticate = new Crypto();
            Connetion = new DBConnect();
            auth = new Authentication();

            if (!Connetion.IsConnectionOk())
            {
                MessageBox.Show("Connection with database could not be established", MessageBoxImage.Warning.ToString(), MessageBoxButton.OK, MessageBoxImage.Warning);
                btnSignup.IsEnabled = false;
                btnLogin.IsEnabled = false;
            }


            lblAuthenticationError.Visibility = Hidden;
        }

        private void txtUsername_LostFocus(object sender, RoutedEventArgs e)
        {
            auth.Username = txtUsername.Text;
        }

        private void passwordBox_LostFocus(object sender, RoutedEventArgs e)
        {
            auth.Password = passwordBox.Password;
        }

        private void btnLogin_Click(object sender, RoutedEventArgs e)
        {
            if (auth.Login())
                MessageBox.Show("Autenticação concluída com suscesso", "Concluído!");
            else
                lblAuthenticationError.Visibility = Visible;
        }
        private void btnSignup_Click(object sender, RoutedEventArgs e)
        {
            SignupWindow signup = new SignupWindow();
            signup.Show();
            this.Close();
        }
    }
}

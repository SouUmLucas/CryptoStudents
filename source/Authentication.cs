﻿using System;
using System.Text;

namespace CryptoStudent
{
    class Authentication
    {
        DBConnect db;
        Crypto hashPassword = new Crypto();
        public string Username { get; set; }
        private string password;
        private byte[] bytePassword;

        public string Password
        {
            get
            {
                return password;
            }
            set
            {
                bytePassword = Encoding.UTF8.GetBytes(value);
                var hashedPassword = (hashPassword.ComputeHashSHA512(bytePassword));
                password = Convert.ToBase64String(hashedPassword);
            }
        }

        public bool Login()
        {
            if (Username != null && Password != null)
            {
                db = new DBConnect();

                return db.Authenticate(Username, Password);
            }
            else
            {
                return false;
            }
        }
    }
}

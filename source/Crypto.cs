﻿using System.Security.Cryptography;

namespace CryptoStudent
{

    class Crypto
    {
        public byte[] ComputeHashSHA512(byte[] toBeHashed)
        {
            using (var sha512 = SHA512.Create())
            {
                return sha512.ComputeHash(toBeHashed);
            }
        }
    }
}

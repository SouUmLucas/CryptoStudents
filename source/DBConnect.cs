﻿using System;
using Npgsql;

namespace CryptoStudent
{
    class DBConnect
    {
        static string serverName = "localhost";
        static string port = "5433";
        static string userName = "postgres";
        static string password = "ubowpxnx";
        static string databaseName = "fatec";
        string connString = null;
        NpgsqlConnection pgsqlConnection;

        /// <summary>
        /// Constructor method set connection string
        /// </summary>
        public DBConnect()
        {
            connString = String.Format("Server={0};Port={1};User Id={2};Password={3};Database={4};", serverName, port, userName, password, databaseName);
        }

        /// <summary>
        /// Method to open connecton with DB
        /// always runs before a query execution
        /// </summary>
        private void OpenConnection()
        {
            try
            {
                pgsqlConnection = new NpgsqlConnection(connString);
                pgsqlConnection.Open();
            }
            catch(NpgsqlException ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Method to close connection with DB
        /// always runs after a query execution
        /// </summary>
        private void CloseConnection()
        {
            try
            {
                pgsqlConnection.Close();
            }
            catch(NpgsqlException ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Method to test connection with PgSql
        /// </summary>
        /// <returns>TRUE if connection is ok; FALSE if db isn't configured properly</returns>
        public bool IsConnectionOk()
        {
            try
            {
                OpenConnection();
            }
            catch
            {
                return false;
            }
            finally
            {
                CloseConnection();
            }
            return true;
        }

        /// <summary>
        /// Method to create a new user
        /// </summary>
        /// <param name="name">Student's name</param>
        /// <param name="username">Student's username</param>
        /// <param name="email">Student's email</param>
        /// <param name="hashedPassword">Student's ciphered password</param>
        public void Insert(string name, string username, string email, string hashedPassword)
        {
            try
            {
                OpenConnection();

                string cmdInsert = String.Format("INSERT INTO alunos (id, name, username, email, password) VALUES (nextval('seq_id'), '{0}', '{1}', '{2}', '{3}')", name, username, email, hashedPassword);

                using (NpgsqlCommand pgsqlCommand = new NpgsqlCommand(cmdInsert, pgsqlConnection))
                {
                    pgsqlCommand.ExecuteNonQuery();

                }
            }
            catch(Exception ex)
            {
                throw ex;
            }
            finally
            {
                CloseConnection();
            }
        }

        /// <summary>
        /// Method to authenticate a user
        /// </summary>
        /// <param name="username">Student's username</param>
        /// <param name="hashedPassword">Student's ciphered password</param>
        /// <returns>TRUE if user entered correctly username and password; FALSE if something got wrong</returns>
        public bool Authenticate(string username, string hashedPassword)
        {
            string storedPassword = "";

            try
            {
                OpenConnection();

                string cmdSelect = String.Format("SELECT password FROM alunos WHERE username = '{0}'", username);

                NpgsqlCommand command = new NpgsqlCommand(cmdSelect, pgsqlConnection);

                NpgsqlDataReader dr = command.ExecuteReader();

                while(dr.Read())
                {
                   storedPassword = String.Format("{0}", dr[0]);
                }
            }
            catch(Exception ex)
            {
                throw ex;
            }
            finally
            {
                CloseConnection();
            }

            if (hashedPassword == storedPassword)
                return true;
            else
                return false;
        }
    }
}

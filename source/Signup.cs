﻿using System;
using System.Text;

namespace CryptoStudent
{
    class Signup
    {
        Crypto hashPassword = new Crypto();
        DBConnect db = new DBConnect();
        public string Email { get; set; }
        public string Name { get; set; }
        public string Username { get; set; }
        private string password;
        private string passwordConfirmation;
        private byte[] bytePassword;
        private byte[] byteConfirmPassword;

        /// <summary>
        /// set the password to a SHA512 hashed password
        /// </summary>
        public string Password
        {
            get
            {
                return password;
            }
            set
            {
                bytePassword = Encoding.UTF8.GetBytes(value);
                var hashedPassword = hashPassword.ComputeHashSHA512(bytePassword);
                password = Convert.ToBase64String(hashedPassword);
            }
        }

        /// <summary>
        /// set the password confirmation to a SHA512 hashed password
        /// </summary>
        public string PasswordConfirmation
        {
            get
            {
                return passwordConfirmation;
            }
            set
            {
                byteConfirmPassword = Encoding.UTF8.GetBytes(value);
                var hashedPassword = hashPassword.ComputeHashSHA512(byteConfirmPassword);
                passwordConfirmation = Convert.ToBase64String(hashedPassword);
            }
        }

        /// <summary>
        /// Method to confirm user subscription
        /// </summary>
        /// <returns>TRUE if signup action was OK; FALSE if something got wrong</returns>
        public bool ConfirmSubscription()
        {
            if (password == passwordConfirmation)
            {
                try
                {
                    db.Insert(Name, Username, Email, this.Password);
                }
                catch
                {
                    return false;
                }

                return true;
            }
            else
            {
                return false;
            }
        }
    }
}
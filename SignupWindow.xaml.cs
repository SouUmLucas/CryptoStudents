﻿using System.Windows;

namespace CryptoStudent
{
    /// <summary>
    /// Interaction logic for Signup.xaml
    /// </summary>
    public partial class SignupWindow : Window
    {
        Signup confirmation;
        MainWindow signin;

        public SignupWindow()
        {
            InitializeComponent();
            confirmation = new Signup();
            signin = new MainWindow();
        }

        private void txtEmail_LostFocus(object sender, RoutedEventArgs e)
        {
            confirmation.Email = txtEmail.Text;
        }

        private void txtName_LostFocus(object sender, RoutedEventArgs e)
        {
            confirmation.Name = txtName.Text;
        }

        private void txtUsername_LostFocus(object sender, RoutedEventArgs e)
        {
            confirmation.Username = txtUsername.Text;
        }

        private void txtPassword_LostFocus(object sender, RoutedEventArgs e)
        {
            if (txtPassword.Password != "")
                confirmation.Password = txtPassword.Password;
        }

        private void txtPasswordConfirmation_LostFocus(object sender, RoutedEventArgs e)
        {
            if (txtPasswordConfirmation.Password != "")
                confirmation.PasswordConfirmation = txtPasswordConfirmation.Password;
        }

        private void btnReady_Click(object sender, RoutedEventArgs e)
        {
            if (confirmation.ConfirmSubscription())
            {
                this.Close();
                signin.Show();
            }
            else
            {
                MessageBox.Show("Confirmação de senha inválida. Corrije-a e tente novamente.", "Erro", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void btnCancel_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
            signin.Show();
        }
    }
}
